import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static boolean checkBoard(char[][] board, int rowLast, int colLast) {
        rowLast--;
        colLast--;
        char symbol = board[rowLast][colLast];
        boolean win = true;
        for(int col = 0; col < 3; col++){
            if (board[rowLast][col] != symbol) {
                win = false;
                break;
            }
        }
        if (win)
			return true;
	
        win = true;
        for(int row = 0; row < 3; row++){
            if (board[row][colLast] != symbol) {
                win = false;
                break;
            }
        }
        if (win) 
            return true;

        if (rowLast - colLast == 0) {
            win = true;
            for (int loc = 0; loc <3; loc++) {
                if (board [loc][loc] != symbol) {
                    win = false;
                    break;
                }
            }
            if (win)
                return true;
        }

        if ( (rowLast == 2 && colLast == 0) || (rowLast == 0 && colLast == 2 ) ) {
            win = true;
            for (int row = 0; row <3; row++) {
                if (board [row][2-row] != symbol) {
                    win = false;
                    break;
                }
            }
            if (win) {
                return true;

            }
        }
        return false;
	}
	
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        int moveCount = 0;
        int currentPlayer = 0;
        while(moveCount < 9){
        printBoard(board);
        int row, col;
        do{      
		System.out.print("Player"+(currentPlayer + 1)+ " please enter row number");
		row = reader.nextInt();
		System.out.print("Player"  +  (currentPlayer + 1)  +  " please enter column number");
		col = reader.nextInt();
        } while(!(row > 0 && row < 4  && col < 4 && col > 0 && board[row - 1][col - 1] == ' '));
        board[row - 1][col - 1] = (currentPlayer == 0 ? 'X' : 'O');
        moveCount++;
        if(checkBoard(board, row, col)){
            printBoard(board);
            System.out.println("Player " + (currentPlayer + 1) + " won!");
            break;
        }
        currentPlayer = (currentPlayer + 1) % 2;
        }

		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
