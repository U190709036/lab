public class FindPrimes {

	public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);
        for(int x=0;x<max;x++){
            int divisor = 2;
            boolean isPrime = true;

            while (divisor < x && isPrime){
                if(x % divisor == 0)
                    isPrime = false;
                divisor++;
            }
            if(isPrime){
                System.out.print(x + ",");
            }
        }
        System.out.println("");
	}
	
	
}