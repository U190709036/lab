public class Main {
    
    
    
    
    
    
    
    
    
    
    
    public static void main(String[] args) {
        Point p = new Point(2, 4);
        Rectangle r = new Rectangle(6, 8, p);
        System.out.println(r.area());
        System.out.println(r.perimeter());
        Point[] points = r.corners();
        for (int i = 0; i < points.length; i++) {
            Point a = points[i];
            System.out.println(a.xCoord + " " + a.yCoord);
        }

        Circle c;
        
        c = new Circle(4,new Point(10,10));
        
        c.radius = 10;

        System.out.println(c.area());
        
        System.out.println(c.perimeter());

        Circle cW = new Circle(4,new Point(17,10));

        System.out.println(c.intersect(cW));

        Circle cX = new Circle(4,new Point(25,10));

        System.out.println(c.intersect(cX));

    }
}