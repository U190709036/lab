public class Rectangle{
int sideA;
int sideB;
Point topLeft;
public Rectangle(int sideA,int sideB,Point topLeft){
this.sideA = sideA;
this.sideB = sideB;
this.topLeft = topLeft;
}


public int area(){
return sideA*sideB;
}
public int perimeter(){
return 2*(sideA+sideB);
}
public Point[] corners(){
Point[] points = new Point[4];
points[0] = topLeft;
points[1] = new Point(topLeft.xCoord + sideA,topLeft.yCoord);
points[2] = new Point(topLeft.xCoord, topLeft.yCoord-sideB);
points[3] = new Point(topLeft.xCoord + sideA,topLeft.yCoord-sideB);  
return points;
} 


}