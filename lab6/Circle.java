public class Circle{
int radius;
Point center;
public Circle(int radius,Point center){
    this.radius = radius;
    this.center = center;    
}
public double area(){
    return Math.PI*radius*radius;
}
public double perimeter(){
    return Math.PI*radius*2;
}
public boolean intersect(Circle c) {
double distance = Math.sqrt(Math.pow(center.xCoord - c.center.xCoord, 2) + Math.pow(center.yCoord - c.center.yCoord, 2));
return (c.radius + radius) > distance;   
}
}